#!/usr/bin/env Rscript

library(EnsDb.Hsapiens.v86)

#setup params for ensembldb
edb <- EnsDb.Hsapiens.v86
seqlevelsStyle(edb) <- "UCSC"

args = commandArgs(trailingOnly=TRUE)

if (length(args)==0) {
  stop("At least one argument must be supplied (gene_name_table).n", call.=FALSE)
} else if (length(args)==1) {
  # default output file
  args[2] = "./ensembldb_out.tsv"
}


# read input data
data <- read.csv(file=args[1], header=FALSE, sep="\t")

query_esnsembldb <- function(genename) {
	df <- transcripts(edb, filter = GeneNameFilter(genename))
	df_out  <-subset(data.frame(df),  tx_biotype == "protein_coding")[c("gene_name","gene_id","tx_id")]
	write.table(df_out, args[2], sep = "\t", col.names = FALSE, 
                row.names = FALSE, quote=FALSE, append=TRUE)	
}

mapply(query_esnsembldb, data[1])
